Lister tous les artistes 

```sql
SELECT * FROM artist;
```

Afficher les titres des albums.

```sql
SELECT title FROM album;
```


Afficher le nom et le compositeur de chaque piste.


```sql
SELECT name, composer from track
```


Afficher tous les albums dont le titre contient la lettre "g".

```sql
SELECT * FROM album WHERE title LIKE '%g%';
```


Afficher les 10 premières pistes triées par nom.

```sql
SELECT name FROM track ORDER BY name ASC LIMIT 10;
```


Afficher le titre de chaque album et le nom de son artiste.


```sql
SELECT album.title, artist.name
FROM album
JOIN artist ON album.artist_id = artist.artist_id;
```



Afficher le nom de chaque piste et le titre de son album.


```sql
SELECT track.name, album.title
FROM track
JOIN album ON track.album_id = album.album_id;
```


Lister les factures avec le nom des clients.

```sql
SELECT invoice, customer.last_name
FROM invoice
JOIN customer ON invoice.customer_id = invoice.customer_id;
```



Afficher le nom de chaque employé et le titre de leur poste.


```sql

SELECT last_name, title from employee 
```

Lister les noms des pistes, leurs albums et leurs artistes.


```sql

SELECT track.name, album.title, artist.name
FROM track
JOIN album ON track.album_id = album.album_id
JOIN artist ON album.artist_id = artist.artist_id;
```


Lister les pistes avec leur genre et leur type de média.


```sql

SELECT track.name, genre.name, media_type.name
FROM track
JOIN genre ON track.genre_id = genre.genre_id
JOIN media_type ON track.media_type_id = media_type.media_type_id ;
```




Lister les détails des lignes de facture, incluant la piste et l'album associé.


```sql

SELECT invoice_line, track.name, album.title
FROM invoice_line
JOIN track ON invoice_line.track_id = track.track_id 
JOIN album ON track.album_id = album.album_id ;
```


Lister les noms des clients, les pistes qu'ils ont achetées et les artistes de ces pistes.


```sql

SELECT invoice, customer.last_name, invoice_line.track_id, track.name
FROM invoice
JOIN customer ON invoice.customer_id = customer.customer_id 
JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id 
JOIN track ON track.track_id = invoice_line.track_id ;
```




Calculer le nombre total de pistes dans chaque album.

```sql

SELECT album.title, COUNT(track.track_id) AS total_tracks
FROM track
JOIN album ON track.album_id = album.album_id
GROUP BY album.title;
```



Calculer la durée totale des pistes dans chaque album.


```sql

SELECT album.title, COUNT(track.track_id) AS total_tracks, SUM(track.milliseconds) AS total_time
FROM track
JOIN album ON track.album_id = album.album_id
GROUP BY album.title;
```




Calculer le montant total des ventes pour chaque client.

```sql

SELECT customer.last_name, SUM(invoice.total) AS total_buy
FROM customer
JOIN invoice ON customer.customer_id = invoice.customer_id 
GROUP BY customer.last_name;
```

Trouver les genres avec le plus grand nombre de pistes.


```sql
SELECT genre.name, COUNT(track.track_id) AS total_tracks
FROM track
JOIN genre ON track.genre_id = genre.genre_id
GROUP BY genre.name
ORDER BY total_tracks DESC;
```


Calculer le nombre de factures par pays.


```sql
SELECT invoice.billing_country, COUNT(invoice.invoice_id) AS total_invoices
FROM invoice
GROUP BY invoice.billing_country;
```


Lister les artistes ayant plus de 10 albums.

```sql
SELECT artist.name, COUNT(album.album_id) AS total_albums
FROM artist
JOIN album ON artist.artist_id = album.artist_id
GROUP BY artist.name
HAVING COUNT(album.album_id) > 10;
```

Trouver les clients ayant dépensé plus de 10.


```sql
SELECT customer.last_name, SUM(invoice.total) AS total_buy
FROM customer
JOIN invoice ON customer.customer_id = invoice.customer_id
GROUP BY customer.first_name, customer.last_name
HAVING SUM(invoice.total) > 10;
```


Lister les albums dont la durée totale dépasse 1 heure.

```sql
SELECT album.title, SUM(track.milliseconds) / 3600000 AS total_hours
FROM album
JOIN track ON album.album_id = track.album_id
GROUP BY album.title
HAVING SUM(track.milliseconds) > 3600000;
```


Trouver les genres ayant vendu plus de 20 pistes.


```sql
SELECT genre.name, COUNT(invoice_line.track_id) AS ttoal_tracks_sold
FROM genre
JOIN track ON genre.genre_id = track.genre_id
JOIN invoice_line ON track.track_id = invoice_line.track_id
GROUP BY genre.name
HAVING COUNT(invoice_line.track_id) > 20;
```





